## **WHAT IS DOCKER?**

Docker provides the ability to package and run an application in a loosely isolated environment called a container. 
It is a program for developers to develop and run applications wth container.

## Docker Vocabulary

**1. Docker Container**  
The container is a block where image is saved.   
Many container can be created using single image.  

**2. Docker Image**  
The Container is a block where all the specifications and enviornment required to run an application is stored.
The image can be employed to any Docker enviornment as container.
* Code.
* Runtime.
* Enviornment.
* Libraries.
* Configuration Files. 

**3. DockerHub**  
It's like Github of Docker.  
Dockerhub stores images and containers. 

 ## Docker Components:  
1. Docker Engine:  
The place where containers and runs the programs.  
2. Docker Client:  
It is the end user which provides the demands of the docker file as commands.  
3. Docker daemon:  
This place checks the client request and communicate with docker components such as image, container, to perform the process.  
4. Docker Registry:  
A place docker images are stored. DockerHub is such a public registry.  

<img src="https://i0.wp.com/cdn-images-1.medium.com/max/1600/1*bIQOZL_pZujjrfaaYlQ_gQ.png?w=810&ssl=1" height="400" width="700">



 ## Basic Docker Commands
1. To pull images from docker repository.
> $ Docker pull (image name)

2. To create container from image.
> $ docker run -it-d (image name)

3. To list the running containers.
> $ docker ps

4. To stop running container
> $ docker stop (container id)

5. To push the images to docker repossitory
> $ docker push (username/imagename) 



